package learn.springboot.employee_rest_api.employee;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    // The following method is responsible to get all employee's details.
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @GetMapping("/employees/{id}")
    // The following method is responsible to get individual employee's details.
    // Requires id.
    public Optional<Employee> getEmployee(@PathVariable int id) {
        return employeeService.getEmployee(id);
    }
    
    @GetMapping("/employees/{id}/salary")
    // The following method is responsible to get the salary of an employee.
    // Requires id.
    public String getSalaryOfAnEmployee(@PathVariable int id) {
        return employeeService.getSalaryOfAnEmployee(id);
    }

    @GetMapping("/employees/greaterThan")
    // The following method is responsible to get the details of those employee whos salary > the provided value.
    // Requires integer value after "/greaterThan?salary=".
    public List<Employee> getEmployeesWithHigherSalary(@RequestParam(defaultValue = "5000") int salary) {
        return employeeService.getEmployeesWithHigherSalary(salary);
    }

    @GetMapping("/employees/lesserThan")
    // The following method is responsible to get the details of those employee who's salary < the provided value.
    // Requires integer value after "/lesserThan?salary=".
    public List<Employee> getEmployeesWithLowerSalary(@RequestParam(defaultValue = "5000") int salary) {
        return employeeService.getEmployeesWithLowerSalary(salary);
    }

    @PostMapping("/employees")
    // The following method is responsible to post new employee details.
    public void addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
    }

    @PutMapping("/employees")
    // The following method is responsible to update an existing employee's detail.
    public void updateEmployee(@RequestBody Employee employee) {
        employeeService.updateEmployee(employee);
    }

    @DeleteMapping("/employees/{id}")
    // The following method is responsible to delete an employee's detail.
    // Requires id.
    public void deleteEmployee(@PathVariable int id) {
        employeeService.deleteEmployee(id);
    }

    @GetMapping("/employees/load")
    public void loadSomeEmployees() {
        employeeService.loadSomeEmployees();
    }

}
