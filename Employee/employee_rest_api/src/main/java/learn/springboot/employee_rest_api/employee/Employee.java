package learn.springboot.employee_rest_api.employee;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employee {
    @Id
    private int id;

    private String name;
    private String mail_Id;
    private int salary;
    
    public Employee() {

    }
    
    public Employee(int id, String name, String mailId, int salary) {
        this.id = id;
        this.name = name;
        this.mail_Id = mailId;
        this.salary = salary;
    }

    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMailId() {
        return mail_Id;
    }
    public void setMailId(String mailId) {
        this.mail_Id = mailId;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    
}
