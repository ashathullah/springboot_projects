package learn.springboot.employee_rest_api.employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    private List<Employee> employees = new ArrayList<>(Arrays.asList(
                new Employee(1, "Thomas", "thomas@gmail.com", 15000),
                new Employee(2, "Author", "author@gmail.com", 12000),
                new Employee(3, "Jhon", "jhon@gmail.com", 10000),
                new Employee(4, "Finn", "finn@gmail.com", 500),
                new Employee(5, "Ada", "ada@gmail.com", 6000),
                new Employee(6, "Polly", "polly@gmail.com", 9000),
                new Employee(7, "charlie", "charlie@gmail.com", 5000),
                new Employee(8, "Curly", "curly@gmail.com", 2000),
                new Employee(9, "Isaiah", "isaiah@gmail.com", 600),
                new Employee(10, "Solomn", "solomn@gmail.com", 1000)
        ));

    public List<Employee> getAllEmployees() {
        List<Employee> employee = new ArrayList<Employee>();
        employeeRepository.findAll()
        .forEach(employee::add);
        return employee;
    }
    
    public Optional<Employee> getEmployee(int id) {
        return employeeRepository.findById(id);
    }

    public String getSalaryOfAnEmployee(int id) {
        String salary = "salary : " + employeeRepository.findById(id).get().getSalary();
        return salary;
    }

    public List<Employee> getEmployeesWithHigherSalary(int salary) {
        List<Employee> employees = new ArrayList<Employee>();
        int index = getAllEmployees().size()-1;
        while(index >= 0) {
            if(getAllEmployees().get(index).getSalary() > salary){
                employees.add(getAllEmployees().get(index));
            }
            index--;
        }
        return employees;
    }

    public List<Employee> getEmployeesWithLowerSalary(int salary) {
        List<Employee> employees = new ArrayList<Employee>();
        int index = getAllEmployees().size()-1;
        while(index >= 0) {
            if(getAllEmployees().get(index).getSalary() < salary){
                employees.add(getAllEmployees().get(index));
            }
            index--;
        }
        return employees;
    }

    public void addEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public void updateEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }

    public void loadSomeEmployees() {
        for(int i = 0; i<10; i++) {
            addEmployee(employees.get(i));
        }
    }

}
