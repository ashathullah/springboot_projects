package com.learning.qrservice.models;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the floor database table.
 * 
 */

public class Floor implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private int active;

	private String count;

	private int enabled;

	private String name;

	private String shortName;

	private Long restaurant;
	
	private long billPrinter;
	
	private long kotPrinter;
	
	private long waiterPrinter;
	
	private long reciptPrinter;
	
	private long orderType;

	private boolean printWaiter;

	private boolean printKot;

	private boolean printBill;

	private boolean printRecipt;

	private List<FloorTable> floorTables;

	public List<FloorTable> getFloorTables() {
		return floorTables;
	}

	public void setFloorTables(List<FloorTable> floorTables) {
		this.floorTables = floorTables;
	}

	public Floor() { }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Long getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Long restaurant) {
		this.restaurant = restaurant;
	}

	public long getBillPrinter() {
		return billPrinter;
	}

	public void setBillPrinter(long billPrinter) {
		this.billPrinter = billPrinter;
	}

	public long getKotPrinter() {
		return kotPrinter;
	}

	public void setKotPrinter(long kotPrinter) {
		this.kotPrinter = kotPrinter;
	}

	public long getWaiterPrinter() {
		return waiterPrinter;
	}

	public void setWaiterPrinter(long waiterPrinter) {
		this.waiterPrinter = waiterPrinter;
	}

	public long getReciptPrinter() {
		return reciptPrinter;
	}

	public void setReciptPrinter(long reciptPrinter) {
		this.reciptPrinter = reciptPrinter;
	}

	public long getOrderType() {
		return orderType;
	}

	public void setOrderType(long orderType) {
		this.orderType = orderType;
	}

	public boolean isPrintWaiter() {
		return printWaiter;
	}

	public void setPrintWaiter(boolean printWaiter) {
		this.printWaiter = printWaiter;
	}

	public boolean isPrintKot() {
		return printKot;
	}

	public void setPrintKot(boolean printKot) {
		this.printKot = printKot;
	}

	public boolean isPrintBill() {
		return printBill;
	}

	public void setPrintBill(boolean printBill) {
		this.printBill = printBill;
	}

	public boolean isPrintRecipt() {
		return printRecipt;
	}

	public void setPrintRecipt(boolean printRecipt) {
		this.printRecipt = printRecipt;
	}
}