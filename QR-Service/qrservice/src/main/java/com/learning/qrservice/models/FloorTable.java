package com.learning.qrservice.models;

import java.io.Serializable;



/**
 * The persistent class for the floor_tables database table.
 * 
 */
public class FloorTable implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private int active;

	private int count;

	private int extaChargeValueType;

	private float extraChargeIncluded;

	private float extraChargeValue;

	private int extraChargesType;

	private String name;

	private String status;
	
	private String occupied;
	
	private String assignedCap;
	
	//bi-directional many-to-one association to Floor
	private Long floor;

	public String getAssignedCap() {
		return assignedCap;
	}

	public void setAssignedCap(String assignedCap) {
		this.assignedCap = assignedCap;
	}

	public FloorTable() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getExtaChargeValueType() {
		return this.extaChargeValueType;
	}

	public void setExtaChargeValueType(int extaChargeValueType) {
		this.extaChargeValueType = extaChargeValueType;
	}

	public float getExtraChargeIncluded() {
		return this.extraChargeIncluded;
	}

	public void setExtraChargeIncluded(float extraChargeIncluded) {
		this.extraChargeIncluded = extraChargeIncluded;
	}

	public float getExtraChargeValue() {
		return this.extraChargeValue;
	}

	public void setExtraChargeValue(float extraChargeValue) {
		this.extraChargeValue = extraChargeValue;
	}

	public int getExtraChargesType() {
		return this.extraChargesType;
	}

	public void setExtraChargesType(int extraChargesType) {
		this.extraChargesType = extraChargesType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getFloor() {
		return this.floor;
	}

	public void setFloor(Long floor) {
		this.floor = floor;
	}
	
	public String getOccupied() {
		return occupied;
	}

	public void setOccupied(String occupied) {
		this.occupied = occupied;
	}



}