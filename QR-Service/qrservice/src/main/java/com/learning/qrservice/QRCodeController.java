package com.learning.qrservice;



// import java.io.File;
// import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
// import com.google.zxing.NotFoundException;
import com.learning.qrservice.models.ObjectDetails;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
// import org.springframework.web.multipart.MultipartFile;

//import com.google.gson.JsonObject;

@RestController
public class QRCodeController {
	
	private static final String QR_CODE_IMAGE_PATH = "D:/java/QRcode.png";//"./src/main/resources/QRCode.png"

	
    @GetMapping(value = "/genrateAndDownloadQRCode/{objectType}/{width}/{height}")
		public Object download(
				@PathVariable("objectType") String objectType,
				@PathVariable("width") Integer width,
				@PathVariable("height") Integer height,
				@RequestBody Object object)
			    throws Exception {
			        return QRCodeGenerator.generateQRCodeImage(objectType,width, height, QR_CODE_IMAGE_PATH, object); 
			    }

    @GetMapping(value = "/genrateQRCode/{objectType}/{width}/{height}")
   	public ResponseEntity<byte[]> generateQRCode(
   			@PathVariable("objectType") String objectType,
   			@PathVariable("width") Integer width,
   			@PathVariable("height") Integer height,
			@RequestBody Object object)
   		    throws Exception {
   		        return ResponseEntity.status(HttpStatus.OK).body(QRCodeGenerator.getQRCodeImage(objectType, width, height, object));
   		    }

	// @GetMapping(value = "/getObjectFromFile")	
	// public ResponseEntity<Object> getObject() throws NotFoundException, IOException {//@RequestParam("image") File imageFile
	// 	return ResponseEntity.status(HttpStatus.OK).body(QRCodeReader.decodeQRCode());//imageFile
	// }

    @GetMapping(value = "/getObject")	
	public ResponseEntity<Object> getObject(@RequestBody ObjectDetails ObjectDetails) throws JsonMappingException, JsonProcessingException {
		return ResponseEntity.status(HttpStatus.OK).body(QRCodeReader.getObject(ObjectDetails));
	}
   	
	// @GetMapping(value = "/getObjectFromString")
	// public ResponseEntity<Object> getObjectFromString(@RequestBody String ObjectDetails) throws JsonMappingException, JsonProcessingException {
	// 	return ResponseEntity.status(HttpStatus.OK).body(QRCodeReader.getObjectFromString(ObjectDetails));
	// }

}
