package com.learning.qrservice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.learning.qrservice.models.ObjectDetails;


public class QRCodeGenerator {

	private static ObjectMapper objectMapper = new ObjectMapper();

	public static BitMatrix QrCodeWriter(String jsonText, int width, int height) throws WriterException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
	    BitMatrix bitMatrix = qrCodeWriter.encode(jsonText, BarcodeFormat.QR_CODE, width, height);
		return bitMatrix;
	}
	
	public static Object generateQRCodeImage(String objectType, int width, int height, String filePath, Object object)
	throws WriterException, IOException {

			String text = objectMapper.writeValueAsString(object);

			ObjectDetails objectDetails = new ObjectDetails(objectType, text);

			String jsonText = objectMapper.writeValueAsString(objectDetails);
			ObjectDetails objectD = objectMapper.readValue(jsonText, ObjectDetails.class);
	        Path path = FileSystems.getDefault().getPath(filePath);
	        MatrixToImageWriter.writeToPath(QrCodeWriter( jsonText, width, height), "PNG", path);
			return objectD;
	    }
		
		
		public static byte[] getQRCodeImage(String objectType, int width, int height, Object object) throws WriterException, IOException {

			String text = objectMapper.writeValueAsString(object);

			ObjectDetails objectDetails = new ObjectDetails(objectType, text);

			String jsonText = objectMapper.writeValueAsString(objectDetails);

		    ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		    MatrixToImageWriter.writeToStream(QrCodeWriter( jsonText, width, height), "PNG", pngOutputStream);
		    byte[] pngData = pngOutputStream.toByteArray();
		    return pngData;
		}

}
