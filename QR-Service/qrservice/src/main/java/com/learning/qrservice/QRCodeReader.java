package com.learning.qrservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.learning.qrservice.models.ObjectDetails;


import com.learning.qrservice.models.Employee;
import com.learning.qrservice.models.FloorTable;
public class QRCodeReader {

    private static ObjectMapper objectMapper = new ObjectMapper();

    // private static final String QR_CODE_IMAGE_PATH = "D:/java/QRcode.png";

    public static Object getObject(ObjectDetails objectDetails) throws JsonMappingException, JsonProcessingException{
        Object object = new Object();
        switch(objectDetails.getobjectType()){
            case "Employee":object = objectMapper.readValue(objectDetails.getJsonString(), Employee.class);
               break;
            case "FloorTable":object = objectMapper.readValue(objectDetails.getJsonString(), FloorTable.class);
                break;
            }
            // Object object = objectMapper.readValue(objectDetails.getJsonString(), Employee.class);
            return object;
    }

    public static Object getObjectFromString(String objectDetails) throws JsonMappingException, JsonProcessingException {

        ObjectDetails objectD = objectMapper.readValue(objectDetails, ObjectDetails.class);

        Object object = objectMapper.readValue(objectD.getJsonString(), Employee.class);
        
        return object;

    }

    @SuppressWarnings("unused")
	private static Object decodeQRCode(File qrCodeimage) throws IOException, NotFoundException {
        // File qrCodeimage = new ClassPathResource(QR_CODE_IMAGE_PATH).getFile();
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        
            Result result = new MultiFormatReader().decode(bitmap);
            return getObjectFromString(result.getText());
    }
}