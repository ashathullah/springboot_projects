package com.learning.qrservice.models;

public class ObjectDetails{

    private String objectType;
    private String jsonString;

    public ObjectDetails(){
    }

    public ObjectDetails(String objectType, String jsonString) {
        this.objectType = objectType;
        this.jsonString = jsonString;
    }

    public String getobjectType() {
        return objectType;
    }

    public void setobjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

}