package com.learning.qrservice;

// import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class QrserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrserviceApplication.class, args);
	}

}
